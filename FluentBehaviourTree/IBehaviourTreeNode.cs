﻿namespace FluentBehaviourTree
{
    /// <summary>
    /// Interface for behaviour tree nodes.
    /// </summary>
    public interface IBehaviourTreeNode
    {
        /// <summary>
        /// Tick providing current game data.
        /// </summary>
        BehaviourTreeStatus Tick(GameData gameData);
    }
}
