﻿using System.Collections.Generic;

namespace FluentBehaviourTree.Nodes
{
    /// <summary>
    /// Selects the first node that succeeds. Tries successive nodes until it finds one that doesn't fail.
    /// </summary>
    public class SelectorNode : IParentBehaviourTreeNode
    {
        /// <summary>
        /// The name of the node.
        /// </summary>
        private readonly string name;

        /// <summary>
        /// List of child nodes.
        /// </summary>
        private readonly List<IBehaviourTreeNode> children;

        public SelectorNode(string name)
        {
            this.name = name;
            this.children = new List<IBehaviourTreeNode>();
        }

        public BehaviourTreeStatus Tick(GameData gameData)
        {
            foreach (var child in children)
            {
                var childStatus = child.Tick(gameData);
                if (childStatus != BehaviourTreeStatus.Failure)
                {
                    return childStatus;
                }
            }

            return BehaviourTreeStatus.Failure;
        }

        /// <summary>
        /// Add a child node to the selector.
        /// </summary>
        public void AddChild(IBehaviourTreeNode child)
        {
            children.Add(child);
        }
    }
}
