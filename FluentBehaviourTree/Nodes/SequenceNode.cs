﻿using System.Collections.Generic;
using System.Linq;

namespace FluentBehaviourTree.Nodes
{
    /// <summary>
    /// Runs child nodes in sequence, until one fails.
    /// </summary>
    public class SequenceNode : IParentBehaviourTreeNode
    {
        /// <summary>
        /// Name of the node.
        /// </summary>
        private readonly string name;

        /// <summary>
        /// List of child nodes.
        /// </summary>
        private readonly List<IBehaviourTreeNode> children;

        public SequenceNode(string name)
        {
            this.name = name;
            this.children = new List<IBehaviourTreeNode>();
        }

        public BehaviourTreeStatus Tick(GameData gameData)
        {
            return children.Select(child => child.Tick(gameData)).FirstOrDefault(childStatus => childStatus != BehaviourTreeStatus.Success);
        }

        /// <summary>
        /// Add a child to the sequence.
        /// </summary>
        public void AddChild(IBehaviourTreeNode child)
        {
            children.Add(child);
        }
    }
}
