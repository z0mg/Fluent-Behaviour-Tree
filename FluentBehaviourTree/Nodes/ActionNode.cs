﻿using System;

namespace FluentBehaviourTree.Nodes
{
    /// <summary>
    /// A behaviour tree leaf node for running an action.
    /// </summary>
    public class ActionNode : IBehaviourTreeNode
    {
        /// <summary>
        /// The name of the node.
        /// </summary>
        private readonly string name;

        /// <summary>
        /// Function to invoke for the action.
        /// </summary>
        private readonly Func<GameData, BehaviourTreeStatus> fn;
        

        public ActionNode(string name, Func<GameData, BehaviourTreeStatus> fn)
        {
            this.name = name;
            this.fn = fn;
        }

        public BehaviourTreeStatus Tick(GameData gameData)
        {
            return fn(gameData);
        }
    }
}
